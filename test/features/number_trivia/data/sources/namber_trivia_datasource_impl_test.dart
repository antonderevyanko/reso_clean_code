import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:reso_coder_trivia/core/error/exceptions.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/sources/number_trivia_local_datasource.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  late MockSharedPreferences mockSharedPreferences;
  late NumberTriviaLocalDataSourceImpl dataSourceImpl;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSourceImpl = NumberTriviaLocalDataSourceImpl(
        sharedPreferences: mockSharedPreferences);
  });

  group('getLastNumberTrivia', () {
    test('should return a trivia if it is cashed', () async {
      when(() => mockSharedPreferences.getString(KEY_TRIVIA))
          .thenReturn(fixture('trivia_cached.json'));

      final cached = fixture('trivia_cached.json');
      final result = await dataSourceImpl.getLastNumberTrivia();
      expect(result, NumberTriviaModel.fromJson(json.decode(cached)));
      verify(() => mockSharedPreferences.getString(KEY_TRIVIA));
    });

    test('should return DataSourceFailure when no data stored', () async {
      when(() => mockSharedPreferences.getString(KEY_TRIVIA)).thenReturn(null);
      final result = dataSourceImpl.getLastNumberTrivia;
      expect(result, throwsA(isA<CacheException>()));
    });
  });

  group('saveNumberTrivia', () {
    final tNumberTriviaModel = NumberTriviaModel(text: 'test text', number: 1);

    test('should call Shared prefs to store the data', () async {
      when(() => mockSharedPreferences.setString(KEY_TRIVIA, any()))
          .thenAnswer((invocation) async => true);
      final expectedJsonString = json.encode(tNumberTriviaModel.toJson());
      dataSourceImpl.cacheNumberTrivia(tNumberTriviaModel);
      verify(() =>
          mockSharedPreferences.setString(KEY_TRIVIA, expectedJsonString));
    });
  });
}
