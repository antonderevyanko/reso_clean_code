import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';
import 'package:reso_coder_trivia/core/error/exceptions.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/sources/number_trivia_remote_datasource.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  late MockHttpClient mockHttpClient;
  late NumberTriviaRemoteDataSourceImpl remoteDataSource;

  void configureErrorResponse(String url) {
    when(() =>
            mockHttpClient.get(Uri.parse(url), headers: any(named: 'headers')))
        .thenAnswer((_) async => http.Response('Something went wrong ', 404));
  }

  void configureOkResponse(String url) {
    when(() =>
            mockHttpClient.get(Uri.parse(url), headers: any(named: 'headers')))
        .thenAnswer((_) async => http.Response(fixture('trivia.json'), 200));
  }

  setUp(() {
    mockHttpClient = MockHttpClient();
    remoteDataSource =
        NumberTriviaRemoteDataSourceImpl(httpClient: mockHttpClient);
  });

  // ========= getConcreteNumberGtrivia
  group('getConcreteNumberGtrivia', () {
    final tNumber = 1;
    final String apiUrl = 'http://numbersapi.com/$tNumber';
    final NumberTriviaModel tNumberTriviaModel =
        NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));

    test(
        'should perfrom a GET request with proper number and application/json header',
        () async {
      configureOkResponse(apiUrl);
      remoteDataSource.getConcreteNumberTrivia(tNumber);
      verify(() => mockHttpClient.get(Uri.parse(apiUrl),
          headers: {'Content-Type': 'application/json'}));
    });

    test('should return a NumberTriviaModel when the response is 200 OK',
        () async {
      configureOkResponse(apiUrl);
      final result = await remoteDataSource.getConcreteNumberTrivia(tNumber);
      expect(result, equals(tNumberTriviaModel));
    });

    test('should return a ServerException when the response is not 200 OK',
        () async {
      configureErrorResponse(apiUrl);
      final call = remoteDataSource.getConcreteNumberTrivia;
      expect(() => call(tNumber), throwsA(isA<ServerException>()));
    });
  });

  // ========= getRandomNumberGtrivia
  group('getRandomNumberGtrivia', () {
    final String apiUrl = 'http://numbersapi.com/random';
    final NumberTriviaModel tNumberTriviaModel =
        NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));

    test(
        'should perfrom a GET request with proper number and application/json header',
        () async {
      configureOkResponse(apiUrl);
      remoteDataSource.getRandomNumberTrivia();
      verify(() => mockHttpClient.get(Uri.parse(apiUrl),
          headers: {'Content-Type': 'application/json'}));
    });

    test('should return a NumberTriviaModel when the response is 200 OK',
        () async {
      configureOkResponse(apiUrl);
      final result = await remoteDataSource.getRandomNumberTrivia();
      expect(result, equals(tNumberTriviaModel));
    });

    test('should return a ServerException when the response is not 200 OK',
        () async {
      configureErrorResponse(apiUrl);
      final call = remoteDataSource.getRandomNumberTrivia;
      expect(() => call(), throwsA(isA<ServerException>()));
    });
  });
}
