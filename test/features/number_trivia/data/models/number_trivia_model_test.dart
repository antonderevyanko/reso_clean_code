import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/entities/number_trivia.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tNumberTriviaModel = NumberTriviaModel(number: 1, text: 'Test Text');

  testWidgets('should be a subclass of TriviaNumber entity', (tester) async {
    expect(tNumberTriviaModel, isA<NumberTriviaModel>());
  });

  group('from json', () {
    test('convert regular trivia response', () async {
      final Map<String, dynamic> jsonMap = json.decode(fixture('trivia.json'));
      final convert = NumberTriviaModel.fromJson(jsonMap);

      expect(convert, tNumberTriviaModel);
    });

    test(
        'should return a valid model when the JSON number is regarded as a double',
        () async {
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('trivia_double.json'));
      final convert = NumberTriviaModel.fromJson(jsonMap);

      expect(convert, tNumberTriviaModel);
    });
  });

  group('to json', () {
    test('should return a JSON map containing the proper data', () async {
      final result = tNumberTriviaModel.toJson();
      final expectedJsonMap = {
        "text": "Test Text",
        "number": 1,
      };
      expect(result, expectedJsonMap);
    });

    test('factory for mapping trivia model to trivia is working', () async {
      final NumberTrivia numberTrivia =
          NumberTrivia.fromModel(tNumberTriviaModel);
      expect(numberTrivia, NumberTrivia.fromModel(tNumberTriviaModel));
    });
  });
}
