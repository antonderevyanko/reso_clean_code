import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:reso_coder_trivia/core/error/failures.dart';
import 'package:reso_coder_trivia/core/usecases/usecase.dart';
import 'package:reso_coder_trivia/core/util/input_converter.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/usecases/get_random_number_trivia.dart';
import 'package:reso_coder_trivia/features/number_trivia/presentation/bloc/number_trivia_bloc.dart';

class MockGetTriviaConcreteNumber extends Mock
    implements GetConcreteNumberTrivia {}

class MockGetRandomNumberTrivia extends Mock implements GetRandomNumberTrivia {}

class MockInputConverter extends Mock implements InputConverter {}

void main() {
  late MockGetTriviaConcreteNumber getConcreteNumber;
  late MockGetRandomNumberTrivia getRandomNumberTrivia;
  late MockInputConverter inputConverter;
  late NumberTriviaBloc numberTriviaBloc;

  setUp(() {
    getRandomNumberTrivia = MockGetRandomNumberTrivia();
    getConcreteNumber = MockGetTriviaConcreteNumber();
    inputConverter = MockInputConverter();
    numberTriviaBloc = NumberTriviaBloc(
        getConcreteNumber: getConcreteNumber,
        getRandomNumber: getRandomNumberTrivia,
        inputConverter: inputConverter);
  });

  tearDown(() {
    numberTriviaBloc.close();
  });

  test('initial block state should be empty', () {
    expect(numberTriviaBloc.state, equals(StateEmpty()));
  });

  group('get trivia concrete number', () {
    final tNumberString = '1';
    final tNumberParsed = 1;
    final tNumberTrivia = NumberTrivia(text: '1', number: 1);

    void setUpSuccesResponse() {
      when(() => inputConverter.stringToUnsignedInt(any()))
          .thenReturn(Right(tNumberParsed));
      when(() => getConcreteNumber(Params(tNumberParsed)))
          .thenAnswer((_) async => Right(tNumberTrivia));
    }

    test(
        'should call InputConverter to validate and convert the string to unsigned int',
        () async {
      setUpSuccesResponse();
      numberTriviaBloc.add(EventGetTriviaConcreteNumber(tNumberString));
      await untilCalled(() => inputConverter.stringToUnsignedInt(any()));
      verify(() => inputConverter.stringToUnsignedInt(tNumberString));
    });

    test('should emit [Error] state if input is invalid', () async {
      when(() => inputConverter.stringToUnsignedInt(any()))
          .thenReturn(Left(InvalidInputFailure()));

      numberTriviaBloc.add(EventGetTriviaConcreteNumber(tNumberString));
      expectLater(numberTriviaBloc.stream,
          emitsInOrder([StateError(message: INVALID_INPUT_FAILURE_MESSAGE)]));
    });

    blocTest(
      'should emit [Error] state if input is invalid',
      build: () {
        when(() => inputConverter.stringToUnsignedInt(any()))
            .thenReturn(Left(InvalidInputFailure()));
        return numberTriviaBloc;
      },
      act: (bloc) => (bloc as NumberTriviaBloc)
          .add(EventGetTriviaConcreteNumber(tNumberString)),
      expect: () => [StateError(message: INVALID_INPUT_FAILURE_MESSAGE)],
    );

    test('should get data from the concrete usecase', () async {
      setUpSuccesResponse();
      numberTriviaBloc.add(EventGetTriviaConcreteNumber(tNumberString));
      await untilCalled(() => getConcreteNumber(Params(tNumberParsed)));
      verify(() => getConcreteNumber(Params(tNumberParsed)));
    });

    blocTest(
      'check with library emits [LoadingnState] with isEmailValid true',
      build: () {
        setUpSuccesResponse();
        return numberTriviaBloc;
      },
      act: (bloc) async =>
          numberTriviaBloc.add(EventGetTriviaConcreteNumber(tNumberString)),
      expect: () => [
        StateLoading(),
        StateLoaded(trivia: tNumberTrivia),
      ],
    );

    test(
      'check old way should emit [Loading, Loaded] when data is gotten successfully',
      () async {
        setUpSuccesResponse();
        final expected = [
          StateLoading(),
          StateLoaded(trivia: tNumberTrivia),
        ];
        numberTriviaBloc.add(EventGetTriviaConcreteNumber(tNumberString));
        expectLater(numberTriviaBloc.stream, emitsInOrder(expected));
      },
    );

    blocTest(
      'should emit [Loading, Error] when getting data fails',
      build: () {
        setUpSuccesResponse();
        when(() => inputConverter.stringToUnsignedInt(any()))
            .thenReturn(Right(tNumberParsed));
        when(() => getConcreteNumber(Params(tNumberParsed)))
            .thenAnswer((_) async => Left(ServerFailure()));
        return numberTriviaBloc;
      },
      act: (bloc) => (bloc as NumberTriviaBloc)
          .add(EventGetTriviaConcreteNumber(tNumberString)),
      expect: () =>
          [StateLoading(), StateError(message: SERVER_FAILURE_MESSAGE)],
    );

    blocTest(
      'should emit [Loading, Error] with correct error message',
      build: () {
        setUpSuccesResponse();
        when(() => inputConverter.stringToUnsignedInt(any()))
            .thenReturn(Right(tNumberParsed));
        when(() => getConcreteNumber(Params(tNumberParsed)))
            .thenAnswer((_) async => Left(CacheFailure()));
        return numberTriviaBloc;
      },
      act: (bloc) => (bloc as NumberTriviaBloc)
          .add(EventGetTriviaConcreteNumber(tNumberString)),
      expect: () =>
          [StateLoading(), StateError(message: CACHE_FAILURE_MESSAGE)],
    );
  });

  group('get trivia random number', () {
    final tNumberTrivia = NumberTrivia(text: '1', number: 1);

    test('should get data from the random usecase', () async {
      when(() => getRandomNumberTrivia(NoParams()))
          .thenAnswer((_) async => Right(tNumberTrivia));
      numberTriviaBloc.add(EventGetTriviaRandomNumber());
      await untilCalled(() => getRandomNumberTrivia(NoParams()));
      verify(() => getRandomNumberTrivia(NoParams()));
    });

    blocTest(
      'check with library emits [LoadingnState] ',
      build: () {
        when(() => getRandomNumberTrivia(NoParams()))
            .thenAnswer((_) async => Right(tNumberTrivia));
        return numberTriviaBloc;
      },
      act: (bloc) async => numberTriviaBloc.add(EventGetTriviaRandomNumber()),
      expect: () => [
        StateLoading(),
        StateLoaded(trivia: tNumberTrivia),
      ],
    );

    test(
      'check old way should emit [Loading, Loaded] when data is gotten successfully',
      () async {
        when(() => getRandomNumberTrivia(NoParams()))
            .thenAnswer((_) async => Right(tNumberTrivia));
        final expected = [
          StateLoading(),
          StateLoaded(trivia: tNumberTrivia),
        ];
        numberTriviaBloc.add(EventGetTriviaRandomNumber());
        expectLater(numberTriviaBloc.stream, emitsInOrder(expected));
      },
    );

    blocTest(
      'should emit [Loading, Error] when getting data fails',
      build: () {
        when(() => getRandomNumberTrivia(NoParams()))
            .thenAnswer((_) async => Left(ServerFailure()));
        return numberTriviaBloc;
      },
      act: (bloc) =>
          (bloc as NumberTriviaBloc).add(EventGetTriviaRandomNumber()),
      expect: () =>
          [StateLoading(), StateError(message: SERVER_FAILURE_MESSAGE)],
    );

    blocTest(
      'should emit [Loading, Error] with correct error message',
      build: () {
        when(() => getRandomNumberTrivia(NoParams()))
            .thenAnswer((_) async => Left(CacheFailure()));
        return numberTriviaBloc;
      },
      act: (bloc) =>
          (bloc as NumberTriviaBloc).add(EventGetTriviaRandomNumber()),
      expect: () =>
          [StateLoading(), StateError(message: CACHE_FAILURE_MESSAGE)],
    );
  });
}
