import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:reso_coder_trivia/core/util/input_converter.dart';

void main() {
  late InputConverter inputConverter;

  setUp(() {
    inputConverter = InputConverter();
  });

  group('stringToUnsignedInt', () {
    test('should convert to int if string is valid', () {
      expect(inputConverter.stringToUnsignedInt('0'), Right(0));
      expect(inputConverter.stringToUnsignedInt('1'), Right(1));
      expect(inputConverter.stringToUnsignedInt('12'), Right(12));
      expect(inputConverter.stringToUnsignedInt('123'), Right(123));
    });

    test('should return proper Failure for not valid string', () {
      expect(
          inputConverter.stringToUnsignedInt(''), Left(InvalidInputFailure()));
      expect(
          inputConverter.stringToUnsignedInt('a'), Left(InvalidInputFailure()));
      expect(inputConverter.stringToUnsignedInt('1.1'),
          Left(InvalidInputFailure()));
    });

    test('should return Failure for a negative number', () {
      expect(inputConverter.stringToUnsignedInt('-1'),
          Left(InvalidInputFailure()));
      expect(inputConverter.stringToUnsignedInt('-12'),
          Left(InvalidInputFailure()));
      expect(inputConverter.stringToUnsignedInt('-1.0'),
          Left(InvalidInputFailure()));
    });
  });
}
