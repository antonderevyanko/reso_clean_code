import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:reso_coder_trivia/core/network/network_info.dart';

void main() {
  late NetworkInfoImpl networkInfo;

  setUp(() {
    networkInfo = NetworkInfoImpl();
  });

  group('isConnected', () {
    test(
      'should forward the call to DataConnectionChecker.hasConnection',
      () async {
        // arrange
        final tHasConnectionFuture = Future.value(true);

        // NOTICE: We're NOT awaiting the result
        final result = await networkInfo.isConnected;
        // assert
        // Only references to the same object are equal.
        expect(result, true);
      },
    );
  });
}
