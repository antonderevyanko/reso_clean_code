import 'package:dartz/dartz.dart';
import 'package:reso_coder_trivia/core/error/failures.dart';

class InputConverter {
  Either<Failure, int> stringToUnsignedInt(String inputString) {
    final result = int.tryParse(inputString);
    if (result != null && result >= 0) {
      return Right(result);
    } else {
      return Left(InvalidInputFailure());
    }
  }
}

class InvalidInputFailure extends Failure {
  @override
  List<Object?> get props => [];
}
