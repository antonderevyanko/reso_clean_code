import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:reso_coder_trivia/core/error/failures.dart';

abstract class Usecase<Type, Params> {
  Future<Either<Failure, Type>> call(Params parameter);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}
