import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:reso_coder_trivia/core/error/failures.dart';
import 'package:reso_coder_trivia/core/usecases/usecase.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/repositories/number_trivia_repository.dart';

class GetConcreteNumberTrivia extends Usecase<NumberTrivia, Params> {
  final NumberTriviaRepository numberTriviaRepository;

  GetConcreteNumberTrivia(this.numberTriviaRepository);

  @override
  Future<Either<Failure, NumberTrivia>> call(Params params) {
    return numberTriviaRepository.getConcreteNumberTrivia(params.number);
  }
}

class Params extends Equatable {
  final int number;

  Params(this.number);

  @override
  List<Object?> get props => [number];
}
