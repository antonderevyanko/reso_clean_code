import 'package:reso_coder_trivia/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:reso_coder_trivia/core/usecases/usecase.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/repositories/number_trivia_repository.dart';

class GetRandomNumberTrivia extends Usecase<NumberTrivia, NoParams> {
  final NumberTriviaRepository numberTriviaRepository;

  GetRandomNumberTrivia(this.numberTriviaRepository);

  @override
  Future<Either<Failure, NumberTrivia>> call(NoParams parameter) {
    return numberTriviaRepository.getRandomNumberTrivia();
  }
}
