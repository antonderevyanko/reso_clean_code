import 'package:equatable/equatable.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/models/number_trivia_model.dart';

class NumberTrivia extends Equatable {
  final String text;
  final int number;

  NumberTrivia({required this.text, required this.number});

  factory NumberTrivia.fromModel(NumberTriviaModel model) {
    return NumberTrivia(text: model.text, number: model.number);
  }
  @override
  List<Object> get props => [text, number];
}
