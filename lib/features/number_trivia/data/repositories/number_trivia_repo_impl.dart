import 'package:reso_coder_trivia/core/error/exceptions.dart';
import 'package:reso_coder_trivia/core/network/network_info.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/sources/number_trivia_local_datasource.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/sources/number_trivia_remote_datasource.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/entities/number_trivia.dart';

import 'package:reso_coder_trivia/core/error/failures.dart';

import 'package:dartz/dartz.dart';

import '../../domain/entities/number_trivia.dart';
import '../../domain/repositories/number_trivia_repository.dart';

typedef Future<NumberTriviaModel> _FutureModel();

class NumberTriviaRepositoryImpl implements NumberTriviaRepository {
  final NumberTriviaRemoteDataSource remoteDataSource;
  final NumberTriviaLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  NumberTriviaRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, NumberTrivia>> getConcreteNumberTrivia(
      int number) async {
    return await _loadTrivia(() {
      return remoteDataSource.getConcreteNumberTrivia(number);
    });
  }

  @override
  Future<Either<Failure, NumberTrivia>> getRandomNumberTrivia() async {
    return await _loadTrivia(() {
      return remoteDataSource.getRandomNumberTrivia();
    });
  }

  Future<Either<Failure, NumberTrivia>> _loadTrivia(
      _FutureModel getConcreteOrRandom) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await getConcreteOrRandom();
        localDataSource.cacheNumberTrivia(response);
        return Right(
            NumberTrivia(number: response.number, text: response.text));
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localTrivia = await localDataSource.getLastNumberTrivia();
        return Right(NumberTrivia.fromModel(localTrivia));
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
