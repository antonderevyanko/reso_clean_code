import 'dart:convert';
import 'dart:io';

import 'package:reso_coder_trivia/core/error/exceptions.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:http/http.dart' as http;

abstract class NumberTriviaRemoteDataSource {
  /// Calls the http://numbersapi.com/{number} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<NumberTriviaModel> getConcreteNumberTrivia(int number);

  /// Calls the http://numbersapi.com/random endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<NumberTriviaModel> getRandomNumberTrivia();
}

class NumberTriviaRemoteDataSourceImpl extends NumberTriviaRemoteDataSource {
  final http.Client httpClient;

  NumberTriviaRemoteDataSourceImpl({required this.httpClient});

  @override
  Future<NumberTriviaModel> getConcreteNumberTrivia(int number) =>
      _loadRemoteTrivia('http://numbersapi.com/$number');

  @override
  Future<NumberTriviaModel> getRandomNumberTrivia() =>
      _loadRemoteTrivia('http://numbersapi.com/random');

  Future<NumberTriviaModel> _loadRemoteTrivia(String apiUrl) async {
    final requestResult = await httpClient
        .get(Uri.parse(apiUrl), headers: {'Content-Type': 'application/json'});
    if (requestResult.statusCode == HttpStatus.ok) {
      return Future.value(
          NumberTriviaModel.fromJson(json.decode(requestResult.body)));
    } else {
      throw ServerException();
    }
  }
}
