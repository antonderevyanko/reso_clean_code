import 'dart:convert';

import 'package:reso_coder_trivia/core/error/exceptions.dart';
import 'package:reso_coder_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class NumberTriviaLocalDataSource {
  // Gets the cached [NumberTriviaModel] which was gotten the last time
  /// the user had an internet connection.
  ///
  /// Throws [NoLocalDataException] if no cached data is present.
  Future<NumberTriviaModel> getLastNumberTrivia();

  Future<void> cacheNumberTrivia(NumberTriviaModel triviaToSave);
}

const KEY_TRIVIA = 'KEY_TRIVIA';

class NumberTriviaLocalDataSourceImpl extends NumberTriviaLocalDataSource {
  final SharedPreferences sharedPreferences;

  NumberTriviaLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<void> cacheNumberTrivia(NumberTriviaModel triviaToSave) {
    sharedPreferences.setString(KEY_TRIVIA, json.encode(triviaToSave));
    return Future.value();
  }

  @override
  Future<NumberTriviaModel> getLastNumberTrivia() {
    final storedRaw = sharedPreferences.getString(KEY_TRIVIA);
    if (storedRaw != null) {
      final decoded = json.decode(storedRaw);
      return Future.value(NumberTriviaModel.fromJson(decoded));
    } else
      throw CacheException();
  }
}
