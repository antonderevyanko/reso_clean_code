part of 'number_trivia_bloc.dart';

abstract class NumberTriviaState extends Equatable {
  const NumberTriviaState();

  @override
  List<Object> get props => [];
}

class StateEmpty extends NumberTriviaState {}

class StateLoading extends NumberTriviaState {}

class StateLoaded extends NumberTriviaState {
  final NumberTrivia trivia;

  StateLoaded({required this.trivia});

  @override
  List<Object> get props => [trivia];
}

class StateError extends NumberTriviaState {
  final String message;

  StateError({required this.message});

  @override
  List<Object> get props => [message];
}
