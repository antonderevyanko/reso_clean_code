import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:reso_coder_trivia/core/error/failures.dart';
import 'package:reso_coder_trivia/core/usecases/usecase.dart';
import 'package:reso_coder_trivia/core/util/input_converter.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
import 'package:reso_coder_trivia/features/number_trivia/domain/usecases/get_random_number_trivia.dart';

part 'number_trivia_event.dart';
part 'number_trivia_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class NumberTriviaBloc extends Bloc<NumberTriviaEvent, NumberTriviaState> {
  final GetConcreteNumberTrivia getConcreteNumber;
  final GetRandomNumberTrivia getRandomNumber;
  final InputConverter inputConverter;

  NumberTriviaBloc(
      {required this.getConcreteNumber,
      required this.getRandomNumber,
      required this.inputConverter})
      : super(StateEmpty());

  @override
  Stream<NumberTriviaState> mapEventToState(
    NumberTriviaEvent event,
  ) async* {
    if (event is EventGetTriviaConcreteNumber) {
      final inputValue = inputConverter.stringToUnsignedInt(event.numberString);
      yield* inputValue.fold((failure) async* {
        yield StateError(message: INVALID_INPUT_FAILURE_MESSAGE);
      }, (numberInt) async* {
        yield StateLoading();
        final failureOrTrivia = await getConcreteNumber(Params(numberInt));
        yield* foldTrivia(failureOrTrivia);
      });
    } else if (event is EventGetTriviaRandomNumber) {
      yield StateLoading();
      final failureOrTrivia = await getRandomNumber(NoParams());
      yield* foldTrivia(failureOrTrivia);
    }
  }

  Stream<NumberTriviaState> foldTrivia(
      Either<Failure, NumberTrivia> failureOrTrivia) async* {
    yield failureOrTrivia.fold(
      (failure) => StateError(message: _mapFailureToMessage(failure)),
      (trivia) => StateLoaded(trivia: trivia),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpacted message';
    }
  }
}
