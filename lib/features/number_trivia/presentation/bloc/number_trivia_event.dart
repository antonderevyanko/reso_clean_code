part of 'number_trivia_bloc.dart';

abstract class NumberTriviaEvent extends Equatable {
  const NumberTriviaEvent();

  @override
  List<Object> get props => [];
}

class EventGetTriviaConcreteNumber extends NumberTriviaEvent {
  final String numberString;

  EventGetTriviaConcreteNumber(this.numberString);
}

class EventGetTriviaRandomNumber extends NumberTriviaEvent {}
